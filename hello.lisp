(defpackage #:hello-ltk
  (:use :cl)
  (:export #:main))

(in-package :hello-ltk)

(defun main ()
  (setf ltk:*debug-tk* nil)
  (ltk:with-ltk ()
    (let* ((f (make-instance
               'ltk:frame
               :padding 5))
           (btn (make-instance
                 'ltk:button
                 :text "Hello World!"
                 :command (lambda ()
                            (ltk:do-msg "Bye!" :title "Hello World!")
                            (setf ltk:*exit-mainloop* t))
                 :master f))
           (txt (make-instance
                 'ltk:label
                 :text "Hello World~!"
                 :master f)))
      (ltk:grid f 0 0)
      (ltk:grid txt 0 0 :sticky "n")
      (ltk:grid btn 1 0 :sticky "s" :padx 42 :pady 24))))
