(defsystem "hello-ltk"
    :description "hello-ltk: a simple GUI-enabled hello word example"
    :version "0.0.1"
    :author "weatherman2095 <weatherman2095@netcourrier.com>"
    :licence "BSD-3"
    :depends-on (:ltk)
    :components ((:file "hello")))
